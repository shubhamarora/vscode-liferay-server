# Change Log

All notable changes to the "Liferay Server Manager (Unofficial)" extension will be documented in this file.

## [0.1.0] - 2021-05-11

### Initial release

- Start/Stop/Add/Delete Liferay Tomcat Server
- Toggle Developer Mode
- Set Custom Java Runtime
- Browse Liferay Server
- Open Tomcat Server Config
- Open Liferay Home
- Linux and Windows Support (MacOS is untested but should work similar to Linux)
- Force Stop Server (Linux Only).
> **Note** Started servers are stopped on closing vscode on windows. On Linux the servers remain active. On reopening vscode and starting server again status is updated.

### Known Issues 
1. While server is starting, liferay server will not stop on exit on **Windows**. On starting vscode again the status is shown as stopped and when starting server again port confict error is shown.
    - **Workaround**: Stop server using `shutdown.bat` or kill the java process from Task Manager
2. Started servers are stopped on closing vscode on **Windows**. On **Linux** the servers remain active. On reopening vscode and starting server again status is updated.

## [0.2.0] - 2021-05-12

### Features
- Deploy jar in liferay server

### Fixes
- stop servers only if server state is started from vscode in windows 
- clear output channel on dispose
- mark extension as preview

### Known Issues 
1. While server is starting, liferay server will not stop on exit on **Windows**. On starting vscode again the status is shown as stopped and when starting server again port confict error is shown.
    - **Workaround**: Stop server using `shutdown.bat` or kill the java process from Task Manager
2. Started servers are stopped on closing vscode on **Windows**. On **Linux** the servers remain active. On reopening vscode and starting server again status is updated.

## [0.3.0] - 2021-05-14

### Features
- Support force stop on windows
- Restore State on windows

## Known Issues 
1. On starting a running server after vscode reload, the logs are read from `liferay.yyyy-mm-dd.log` on windows. The log file is not updated on date change.
    - **Workaround**: Reload vscode and start server again.
