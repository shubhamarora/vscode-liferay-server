# Liferay Server Manager (Unofficial) README

Manage Liferay Server from Visual Studio Code.

## Features

1. Start/Stop/Add/Delete Liferay Tomcat Server
2. Toggle Developer Mode
3. Set Custom Java Runtime
4. Browse Liferay Server
5. Open Tomcat Server Config
6. Open Liferay Home
7. Deploy Jar in Liferay Server
8. Linux and Windows Support (MacOS is untested but should work similar to Linux)
9. Force Stop Server

> **Note** By default, server remain active on closing vscode. State will be restored on starting server again.

## Requirements

- JDK >=8
- Liferay Server >= 7.0 (Tomcat Only)

## Extension Settings

This extension contributes the following settings:

* `liferay.server.showDeveloperModeToggleMessage`: enable/disable information message on toggling developer mode

### Java Runtime Priority Order (1 - highest, 4 - lowest)
1. Java Runtime set after adding Liferay server (Recommended)
2. `java.configuration.runtimes` setting in vscode (This configuration is included in `Language Support for Java(TM) by Red Hat`. Default value is picked.)
3. `java.home` setting in vscode
4. `JAVA_HOME` Environment Variable

## Known Issues 
1. On starting a running server after vscode reload, the logs are read from `liferay.yyyy-mm-dd.log` on windows. The log file is not updated on date change.
    - **Workaround**: Reload vscode and start server again.

## Release Notes

[Liferay Server Manager Release Notes](CHANGELOG.md)
