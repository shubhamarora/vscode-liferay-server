import { commands, ExtensionContext, window } from 'vscode';
import { Commands } from './Constants/Commands';
import { LiferayController } from './Liferay/LiferayController';
import { LiferayServer } from './Liferay/LiferayServer';
import { LiferayServerModel } from './Liferay/LiferayServerModel';
import { LiferayServerTreeProvider } from './Liferay/LiferayServerTreeProvider';

export function activate(context: ExtensionContext) {
    const storagePath: string = setupStorage(context);
    const [liferayServerTree, liferayController]: (LiferayServerTreeProvider | LiferayController)[] = initializeUi(
        context,
        storagePath,
    );
    subscribeCommands(context, liferayServerTree, liferayController);
}

function setupStorage(context: ExtensionContext): string {
    const storagePath: string = context.storageUri?.fsPath || context.globalStorageUri.fsPath;
    return storagePath;
}

function initializeUi(context: ExtensionContext, storagePath: string): [LiferayServerTreeProvider, LiferayController] {
    const liferayServerModel: LiferayServerModel = new LiferayServerModel(storagePath);
    const liferayServerTree: LiferayServerTreeProvider = new LiferayServerTreeProvider(context, liferayServerModel);
    const liferayController: LiferayController = new LiferayController(liferayServerModel);

    context.subscriptions.push(liferayController);
    context.subscriptions.push(liferayServerTree);
    context.subscriptions.push(window.registerTreeDataProvider('liferayServerExplorer', liferayServerTree));
    return [liferayServerTree, liferayController];
}

function subscribeCommands(
    context: ExtensionContext,
    liferayServerTree: LiferayServerTreeProvider,
    liferayController: LiferayController,
): void {
    // Global Pallete Commands
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_ADD, async () => liferayController.addServer()),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_RENAME, async (server: LiferayServer) =>
            liferayController.renameServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DELETE, async (server: LiferayServer) =>
            liferayController.deleteServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_START, async (server: LiferayServer) =>
            liferayController.startServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_STOP, async (server: LiferayServer) =>
            liferayController.stopServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_FORCE_STOP, async (server: LiferayServer) =>
            liferayController.stopServer(server, true),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_BROWSE, async (server: LiferayServer) =>
            liferayController.browseServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_CONFIGURATION, async (server: LiferayServer) =>
            liferayController.openServerConfiguration(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DEVELOPER_MODE, async (server: LiferayServer) =>
            liferayController.toggleDeveloperMode(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_OPEN_HOME_DIR, async (server: LiferayServer) =>
            liferayController.openLifearyHome(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_SET_JAVA_RUNTIME, async (server: LiferayServer) =>
            liferayController.setJavaRuntime(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DEPLOY_JAR, async (server: LiferayServer) =>
            liferayController.deployJar(server),
        ),
    );
    
    // Tree Item Context Menu Commands
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_RENAME_CONTEXT, async (server: LiferayServer) =>
            liferayController.renameServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DELETE_CONTEXT, async (server: LiferayServer) =>
            liferayController.deleteServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_START_CONTEXT, async (server: LiferayServer) =>
            liferayController.startServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_STOP_CONTEXT, async (server: LiferayServer) =>
            liferayController.stopServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_FORCE_STOP_CONTEXT, async (server: LiferayServer) =>
            liferayController.stopServer(server, true),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_BROWSE_CONTEXT, async (server: LiferayServer) =>
            liferayController.browseServer(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_CONFIGURATION_CONTEXT, async (server: LiferayServer) =>
            liferayController.openServerConfiguration(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DEVELOPER_MODE_CONTEXT, async (server: LiferayServer) =>
            liferayController.toggleDeveloperMode(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_OPEN_HOME_DIR_CONTEXT, async (server: LiferayServer) =>
            liferayController.openLifearyHome(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_SET_JAVA_RUNTIME_CONTEXT, async (server: LiferayServer) =>
            liferayController.setJavaRuntime(server),
        ),
    );
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_SERVER_DEPLOY_JAR_CONTEXT, async (server: LiferayServer) =>
            liferayController.deployJar(server),
        ),
    );

    // TreeView Commands
    context.subscriptions.push(
        commands.registerCommand(Commands.LIFERAY_TREE_REFRESH, async (server: LiferayServer) =>
            liferayServerTree.refresh(server),
        ),
    );
}

export function deactivate() {}
