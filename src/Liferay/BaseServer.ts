import { ensureFileSync } from 'fs-extra';
import { join } from 'path';
import { createInterface } from 'readline';
import {
    commands,
    MarkdownString,
    OutputChannel,
    QuickPickItem,
    Terminal,
    TreeItem,
    TreeItemCollapsibleState,
    window,
} from 'vscode';
import { Commands } from '../Constants/Commands';
import {
    INCLUDE_AND_OVERRIDE_PROPERTY,
    PID_TXT,
    PORTAL_DEVELOPER_PROPERTIES,
    PORTEL_EXT_PROPERTIES,
} from '../Constants/Constants';
import { LiferayBundleType, ServerState } from '../Constants/Enums';
import { StringConstants } from '../Constants/StringConstants';
import { CommonUtils } from '../Utils/CommonUtils';
import { FileUtils } from '../Utils/FileUtils';
import { VscodeUtils } from '../Utils/VscodeUtils';
import TailFile = require('@logdna/tail-file');

export abstract class BaseServer extends TreeItem implements QuickPickItem {
    label: string;
    description: string;
    interval: NodeJS.Timeout | undefined;

    #state: ServerState = ServerState.STOPPED;
    #configurationPath: string = '';
    #portalExtPropertiesPath: string;

    #outputChannel!: OutputChannel;
    #terminal!: Terminal;
    #tail: TailFile | undefined;

    constructor(
        private _name: string,
        private _serverInstallPath: string,
        private _storagePath: string,
        private _developerMode: boolean,
        private _bundleType: string,
        private _javaRuntime: string = '',
    ) {
        super(_name, TreeItemCollapsibleState.None);
        let bundleTypes: string[] = Object.values(LiferayBundleType);
        if (bundleTypes.indexOf(_bundleType) >= 0) {
            this._bundleType = _bundleType;
        } else {
            throw Error(StringConstants.ERROR_INVALID_BUNDLE_TYPE);
        }

        this.label = _name;
        this.description = this.#state;
        this.#toggleDeveloperModeIcon();
        this.#portalExtPropertiesPath = join(this._serverInstallPath, PORTEL_EXT_PROPERTIES);

        if (this._developerMode) {
            if (!this.#isDeveloperModeEnabled()) {
                this.#enableDeveloperMode();
            }
        } else {
            if (this.#isDeveloperModeEnabled()) {
                this.#disableDeveloperMode();
            }
        }
    }

    get name(): string {
        return this._name;
    }

    set name(newName: string) {
        this._name = newName;
        this.label = newName;
    }

    get bundleType(): string {
        return this._bundleType;
    }

    set bundleType(type: string) {
        this._bundleType = type;
    }

    get javaRuntime(): string {
        return this._javaRuntime;
    }

    set javaRuntime(_runtime: string) {
        this._javaRuntime = _runtime;
    }

    get started() {
        return this.#state === ServerState.STARTED;
    }

    set state(_state: ServerState) {
        this.#state = _state;
        this.description = this.#state;
        commands.executeCommand(Commands.LIFERAY_TREE_REFRESH);
    }

    get state(): ServerState {
        return this.#state;
    }

    get serverConfigPath(): string {
        return this.#configurationPath;
    }

    protected set serverConfigPath(_configurationPath: string) {
        this.#configurationPath = _configurationPath;
    }

    get serverInstallPath(): string {
        return this._serverInstallPath;
    }

    get extensionStoragePath(): string {
        return this._storagePath;
    }

    get pidFile(): string {
        const path = join(this._storagePath, PID_TXT);
        ensureFileSync(path);
        return path;
    }

    get developerMode(): boolean {
        return this._developerMode;
    }

    #toggleDeveloperModeIcon(): void {
        const icon = this._developerMode ? ' $(wrench) ' : '';
        this.tooltip = new MarkdownString(
            icon + [LiferayBundleType.TOMCAT, '-', this._serverInstallPath].join(' '),
            true,
        );
        commands.executeCommand(Commands.LIFERAY_TREE_REFRESH);
    }

    async toggleDeveloperMode(): Promise<void> {
        this._developerMode = !this._developerMode;
        this.#toggleDeveloperModeIcon();
        if (this._developerMode) {
            if (!this.#isDeveloperModeEnabled()) {
                this.#enableDeveloperMode();
                if (VscodeUtils.shouldShowDeveloperModeInformationMessage()) {
                    const confirmation = await window.showInformationMessage(
                        StringConstants.toggleDeveloperModeMessage(StringConstants.ENABLED, this._name),
                        StringConstants.OK,
                        StringConstants.DO_NOT_SHOW_AGAIN,
                    );

                    if (confirmation && confirmation === StringConstants.DO_NOT_SHOW_AGAIN) {
                        VscodeUtils.disableDeveloperModeToggleMessage();
                    }
                }
            }
        } else {
            if (this.#isDeveloperModeEnabled()) {
                this.#disableDeveloperMode();
                if (VscodeUtils.shouldShowDeveloperModeInformationMessage()) {
                    const confirmation = await window.showInformationMessage(
                        StringConstants.toggleDeveloperModeMessage(StringConstants.DISABLED, this._name),
                        StringConstants.OK,
                        StringConstants.DO_NOT_SHOW_AGAIN,
                    );

                    if (confirmation && confirmation === StringConstants.DO_NOT_SHOW_AGAIN) {
                        VscodeUtils.disableDeveloperModeToggleMessage();
                    }
                }
            }
        }
    }

    getOutputChannel(outputChannelName: string): OutputChannel {
        this.#outputChannel = this.#outputChannel || window.createOutputChannel(outputChannelName);
        return this.#outputChannel;
    }

    getTerminal(terminalName: string): Terminal {
        this.#terminal = this.#terminal || (<any>window).createTerminal({ name: `${terminalName}` });
        return this.#terminal;
    }

    disposeOutputChannel(): void {
        this.#tail && this.#tail.quit();
        if (this.#outputChannel) {
            this.#outputChannel.clear();
            this.#outputChannel.dispose();
        }
    }

    protected async streamServerLogs(_logFile: string): Promise<void> {
        if (!this.#tail) {
            this.#tail = new TailFile(_logFile, { pollFileIntervalMs: 100 }).on(
                'tail_error',
                (error: TypeError | RangeError) => {
                    console.error('TailFile had an error!', error.message);
                },
            );

            try {
                await this.#tail.start();
                const linesplitter = createInterface({
                    input: this.#tail,
                });

                linesplitter.on('line', (line: string) => {
                    this.#outputChannel.appendLine(line);
                });
            } catch (error: unknown) {
                console.error('Cannot start log streaming. Ensure that the file Exists.', error);
            }
        }
    }

    #isDeveloperModeEnabled(): boolean {
        return [...FileUtils.readFileAsArraySync(this.#portalExtPropertiesPath)]
            .map(_ => CommonUtils.removeWhitespaces(_))
            .includes(`${INCLUDE_AND_OVERRIDE_PROPERTY}=${PORTAL_DEVELOPER_PROPERTIES}`);
    }

    #enableDeveloperMode(): void {
        const lines: string[] = FileUtils.readFileAsArraySync(this.#portalExtPropertiesPath);
        const n: number = [...lines]
            .map(_ => CommonUtils.removeWhitespaces(_))
            .indexOf(`${INCLUDE_AND_OVERRIDE_PROPERTY}=`);
        if (n !== -1) {
            lines[n] = `${INCLUDE_AND_OVERRIDE_PROPERTY}=${PORTAL_DEVELOPER_PROPERTIES}`;
        } else {
            lines.push(`${INCLUDE_AND_OVERRIDE_PROPERTY}=${PORTAL_DEVELOPER_PROPERTIES}`);
        }
        FileUtils.writeFileFromArraySync(this.#portalExtPropertiesPath, lines);
    }

    #disableDeveloperMode(): void {
        const lines: string[] = FileUtils.readFileAsArraySync(this.#portalExtPropertiesPath);
        const n: number = [...lines]
            .map(_ => CommonUtils.removeWhitespaces(_))
            .indexOf(`${INCLUDE_AND_OVERRIDE_PROPERTY}=${PORTAL_DEVELOPER_PROPERTIES}`);
        lines[n] = `${INCLUDE_AND_OVERRIDE_PROPERTY}=`;
        FileUtils.writeFileFromArraySync(this.#portalExtPropertiesPath, lines);
    }
    protected abstract browseServer(): Promise<void>;
    protected abstract getJVMOptions(): string[];
    protected abstract setupServer(): void;
    protected abstract startServer(_operatingSystem: string): Promise<boolean>;
    protected abstract stopServer(_operatingSystem: string, _force: boolean): Promise<void>;
}
