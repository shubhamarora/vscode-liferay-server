import { ensureFile, remove } from 'fs-extra';
import { get } from 'http';
import { join } from 'path';
import { check } from 'tcp-port-used';
import { URL } from 'url';
import { OutputChannel, Terminal, window } from 'vscode';
import {
    CATALINA_OUT,
    CONF_DIR,
    DARWIN,
    INTERVAL_5000,
    JAVA_EXECUTABLE_PATH,
    LINUX,
    LOCALHOST,
    LOCALHOST_IP,
    LOGS_DIR,
    PID_TXT,
    SERVER_XML,
    WINDOWS,
} from '../Constants/Constants';
import { LiferayBundleType, LiferayEvent, PortKind, ServerState } from '../Constants/Enums';
import { StringConstants } from '../Constants/StringConstants';
import { JavaUtils } from '../Utils/JavaUtils';
import { LiferayServerUtils } from '../Utils/LiferayServerUtils';
import { NetworkUtils } from '../Utils/NetworkUtils';
import { TomcatUtils } from '../Utils/TomcatUtils';
import { BaseServer } from './BaseServer';
import open = require('open');
import { type } from 'os';

export class LiferayServer extends BaseServer {
    #tomcatPath: string | undefined;
    #tomcatLogPath: string | undefined;

    constructor(
        _name: string,
        _serverInstallPath: string,
        _storagePath: string,
        _developerMode: boolean,
        _bundleType: string,
        _javaRuntime: string,
    ) {
        super(_name, _serverInstallPath, _storagePath, _developerMode, _bundleType, _javaRuntime);
        this.setupServer();
    }

    get outputChannel(): OutputChannel {
        return super.getOutputChannel(`Liferay Server: ${this.name}`);
    }

    get terminal(): Terminal {
        return super.getTerminal(`Liferay Server: ${this.name}`);
    }

    get tomcatPath(): string {
        if (this.#tomcatPath) {
            return this.#tomcatPath;
        }
        throw Error(`Tomcat Directory not found for the server ${this.name}`);
    }

    static async addServer(
        _liferayServerInstallPath: string,
        _serverStoragePath: string,
        _serverName: string,
        _bundleType: string,
    ): Promise<LiferayServer | undefined> {
        if (_bundleType === LiferayBundleType.TOMCAT) {
            await ensureFile(join(_serverStoragePath, PID_TXT));

            if (!(await LiferayServerUtils.isValidLiferayBundle(_liferayServerInstallPath))) {
                throw Error(StringConstants.ENSURE_VALID_LIFERAY_SERVER_DIR);
            }

            try {
                const server: LiferayServer = new LiferayServer(
                    _serverName,
                    _liferayServerInstallPath,
                    _serverStoragePath,
                    false,
                    _bundleType,
                    '',
                );
                return server;
            } catch (error: any) {
                if (error instanceof Error) {
                    console.error(error.message);
                }
                await remove(_serverStoragePath);
                throw Error(error);
            }
        }
        return;
    }

    async browseServer(): Promise<void> {
        if (this.bundleType === LiferayBundleType.TOMCAT) {
            const httpPort: string | undefined = await TomcatUtils.getPort(this.serverConfigPath, PortKind.HTTP);
            httpPort && open(new URL(`http://${LOCALHOST}:${httpPort}`).toString());
        }
    }

    getJVMOptions(): string[] {
        let jvmOptions: string[] = [];
        if (this.bundleType === LiferayBundleType.TOMCAT) {
            jvmOptions = TomcatUtils.getDefaultJVMOptions(this.tomcatPath);
        }
        return jvmOptions;
    }

    setupServer(): void {
        if (this.bundleType === LiferayBundleType.TOMCAT) {
            TomcatUtils.getTomcatDirectoryPath(super.serverInstallPath).then(dir => {
                this.#tomcatPath = dir;
                if (dir) {
                    super.serverConfigPath = join(dir, CONF_DIR, SERVER_XML);
                    if (type() === WINDOWS) {
                        // FIXME: Handle Timezones passed by the user and log file change
                        const today = new Date();
                        const fileName =
                            'liferay.' +
                            today.getUTCFullYear() +
                            '-' +
                            ("0" + (today.getUTCMonth() + 1)).slice(-2) +
                            '-' +
                            today.getUTCDate() +
                            '.log';
                        this.#tomcatLogPath = join(this.serverInstallPath, 'logs', fileName);
                    } else {
                        this.#tomcatLogPath = join(dir, LOGS_DIR, CATALINA_OUT);
                    }
                } else {
                    throw Error(`Tomcat Directory not found for the server ${this.name}`);
                }
            });
        }
    }

    async streamServerLogs(): Promise<void> {
        if (this.#tomcatLogPath) {
            await ensureFile(this.#tomcatLogPath);
            await super.streamServerLogs(this.#tomcatLogPath);
        }
    }

    async startServer(_operatingSystem: string): Promise<boolean> {
        try {
            let [httpPort, httpsPort, serverPort]: (string | undefined)[] = [];

            if (this.bundleType === LiferayBundleType.TOMCAT) {
                const ports: (string | undefined)[] | undefined = await LiferayServerUtils.getLiferayPorts(
                    this.name,
                    this.serverConfigPath,
                    this.bundleType,
                );
                if (!ports) {
                    return false;
                }

                [httpPort, httpsPort, serverPort] = ports;

                if (_operatingSystem === LINUX || _operatingSystem === DARWIN) {
                    if (this.interval) {
                        this.state = ServerState.STOPPED;
                        clearInterval(this.interval);
                    }

                    if (await LiferayServerUtils.checkTomcatProcessByPid(this.pidFile, this.outputChannel)) {
                        this.state = ServerState.STARTED;
                    }

                    if (this.started) {
                        window.showInformationMessage(StringConstants.LIFERAY_SERVER_STARTED);
                        this.outputChannel.show();
                        this.streamServerLogs();
                        return false;
                    }
                } else if (_operatingSystem === WINDOWS) {
                    if (this.started) {
                        window.showInformationMessage(StringConstants.LIFERAY_SERVER_STARTED);
                        return false;
                    }

                    const javaProcessIds = await NetworkUtils.getJavaProcessIDonWindows(this.tomcatPath);
                    if (javaProcessIds && javaProcessIds.length > 0) {
                        this.state = ServerState.STARTED;
                    }

                    if (this.started) {
                        window.showInformationMessage(StringConstants.LIFERAY_SERVER_STARTED);
                        this.outputChannel.show();
                        this.outputChannel.appendLine(
                            `Tomcat appears to still be running with PIDs: (${javaProcessIds.join(
                                ', ',
                            )}). Start aborted.`,
                        );
                        this.outputChannel.appendLine(
                            'If the above process is not a Tomcat process, force stop the server and try again:',
                        );
                        this.streamServerLogs();
                        return false;
                    }
                }

                if (
                    await NetworkUtils.verifyPorts(
                        [
                            { port: httpPort, portKind: PortKind.HTTP },
                            { port: httpsPort, portKind: PortKind.HTTPS },
                            { port: serverPort, portKind: PortKind.SERVER },
                        ],
                        this,
                    )
                ) {
                    return false;
                }

                this.outputChannel.clear();

                let javaPath: string | undefined = this.javaRuntime;
                if (javaPath !== undefined && javaPath.trim() === '') {
                    javaPath = JavaUtils.getJavaExecutable(this.outputChannel);
                    if (javaPath === undefined || (javaPath && javaPath === '')) {
                        window.showErrorMessage(StringConstants.ERROR_JAVA_HOME);
                        return false;
                    }
                } else {
                    this.outputChannel.appendLine(`Using JAVA Executable from: ${javaPath}`);

                    javaPath = join(javaPath, JAVA_EXECUTABLE_PATH);
                }

                await LiferayServerUtils.executeLiferayTomcatBundleCommand(
                    LiferayEvent.START,
                    _operatingSystem,
                    undefined,
                    this,
                    javaPath,
                );

                _operatingSystem === LINUX || _operatingSystem === DARWIN ? await this.streamServerLogs() : null;
                this.state = ServerState.STARTING;
            }

            this.interval = setInterval(() => {
                get(
                    {
                        hostname: LOCALHOST,
                        port: httpPort,
                        path: '/',
                        agent: false,
                    },
                    (res: any) => {
                        if (res.statusCode === 200) {
                            if (this.interval) {
                                clearInterval(this.interval);
                            }
                            this.state = ServerState.STARTED;
                        }
                    },
                );
            }, INTERVAL_5000);
        } catch (error: any) {
            if (error instanceof Error) {
                if (error.message.trim() === 'kill ESRCH') {
                    this.state = ServerState.STOPPED;
                    console.log('Server force killed');
                } else {
                    window.showWarningMessage(StringConstants.ERROR_START_SERVER);
                    console.error(error.message);
                }
            }
        }

        return true;
    }

    async stopServer(_operatingSystem: string, _force: boolean, _javaProcessIds?: number[]): Promise<void> {
        try {
            let argument: string | undefined;
            let event: LiferayEvent = LiferayEvent.STOP;

            if (this.bundleType === LiferayBundleType.TOMCAT) {
                if (_operatingSystem === LINUX || _operatingSystem === DARWIN) {
                    if (_force) {
                        event = LiferayEvent.FORCE_STOP;
                        argument = '-force';
                    }

                    this.interval && clearInterval(this.interval);

                    this.interval = setInterval(async () => {
                        const process = await LiferayServerUtils.checkTomcatProcessByPid(
                            this.pidFile,
                            this.outputChannel,
                            true,
                        );
                        if (!process && this.interval) {
                            this.state = ServerState.STOPPED;
                            clearInterval(this.interval);
                        }
                    }, INTERVAL_5000);
                } else {
                    if (_operatingSystem === WINDOWS) {
                        if (_force && _javaProcessIds && _javaProcessIds.length > 0) {
                            let forceStopped = true;
                            _javaProcessIds.forEach(pid => {
                                forceStopped = process.kill(pid) && forceStopped;
                                console.debug('Process Killed: ' + pid);
                            });

                            if (!forceStopped) {
                                window.showErrorMessage(StringConstants.ERROR_FORCE_STOP);
                            }
                            this.state = ServerState.STOPPED;
                            return;
                        }

                        this.interval && clearInterval(this.interval);

                        this.interval = setInterval(async () => {
                            const httpPort: string | undefined = await TomcatUtils.getPort(
                                this.serverConfigPath,
                                PortKind.HTTP,
                            );

                            let inUse = await check(Number.parseInt(httpPort!), LOCALHOST_IP);

                            if (!inUse && this.interval) {
                                this.state = ServerState.STOPPED;
                                clearInterval(this.interval);
                            }
                        }, INTERVAL_5000);
                    }
                }

                let javaPath: string | undefined = this.javaRuntime;
                if (javaPath !== undefined && javaPath.trim() === '') {
                    javaPath = JavaUtils.getJavaExecutable(this.outputChannel);
                    if (javaPath === undefined || (javaPath && javaPath === '')) {
                        window.showErrorMessage(StringConstants.ERROR_JAVA_HOME);
                        return;
                    }
                } else {
                    this.outputChannel.appendLine(`Using JAVA Executable from: ${javaPath}`);

                    javaPath = join(javaPath, JAVA_EXECUTABLE_PATH);
                }

                await LiferayServerUtils.executeLiferayTomcatBundleCommand(
                    event,
                    _operatingSystem,
                    argument,
                    this,
                    javaPath,
                );

                _operatingSystem === LINUX || _operatingSystem === DARWIN ? await this.streamServerLogs() : null;
                this.outputChannel.show();
                this.state = ServerState.STOPPING;
            }
        } catch (error: any) {
            if (error instanceof Error) {
                window.showWarningMessage(StringConstants.ERROR_START_SERVER);
                console.error(error.message);
            }
        }
    }
}
