import { Event, EventEmitter, ExtensionContext, ThemeIcon, TreeDataProvider, TreeItem } from 'vscode';
import { ServerState } from '../Constants/Enums';
import { LiferayServer } from './LiferayServer';
import { LiferayServerModel } from './LiferayServerModel';

export class LiferayServerTreeProvider implements TreeDataProvider<TreeItem> {
    #onDidChangeTreeData: EventEmitter<TreeItem> = new EventEmitter<TreeItem>();
    readonly onDidChangeTreeData: Event<TreeItem> = this.#onDidChangeTreeData.event;

    constructor(private _context: ExtensionContext, private _liferayServerModel: LiferayServerModel) {}

    async getTreeItem(element: TreeItem): Promise<TreeItem> {
        return element;
    }

    refresh(element: TreeItem): void {
        this.#onDidChangeTreeData.fire(element);
    }

    async getChildren(element?: TreeItem): Promise<TreeItem[]> {
        if (!element) {
            return this._liferayServerModel.serverSet.map((server: LiferayServer) => {
                switch (server.state) {
                    case ServerState.STOPPED:
                        server.iconPath = new ThemeIcon('stop-circle');
                        break;
                    case ServerState.STARTED:
                        server.iconPath = new ThemeIcon('play');
                        break;
                    case ServerState.STARTING:
                        server.iconPath = new ThemeIcon('loading~spin');
                        break;
                    case ServerState.STOPPING:
                        server.iconPath = new ThemeIcon('loading~spin');
                        break;
                }
                server.contextValue = server.state;
                return server;
            });
        }
        return [];
    }

    dispose(): void {}
}
