import { copyFile, pathExistsSync, remove } from 'fs-extra';
import { type } from 'os';
import { join } from 'path';
import { commands, QuickPickItem, Uri, window, workspace } from 'vscode';
import { Commands } from '../Constants/Commands';
import { DEPLOY_DIR, JAVA_EXECUTABLE_PATH, WINDOWS } from '../Constants/Constants';
import { LiferayBundleType, ServerState } from '../Constants/Enums';
import { StringConstants } from '../Constants/StringConstants';
import { CommonUtils } from '../Utils/CommonUtils';
import { LiferayServerUtils } from '../Utils/LiferayServerUtils';
import { NetworkUtils } from '../Utils/NetworkUtils';
import { VscodeUtils } from '../Utils/VscodeUtils';
import { LiferayServer } from './LiferayServer';
import { LiferayServerModel } from './LiferayServerModel';

export class LiferayController {
    #operatingSystem: string = type();
    constructor(private _liferayModel: LiferayServerModel) {}

    async addServer(): Promise<LiferayServer | undefined> {
        let serverStoragePath: string | undefined;
        try {
            const bundleType: string = await this.#selectBundleTypes();
            const liferayInstallPath: string = await this.#selectLiferayInsatllPath();

            const existingServerNames: string[] = this._liferayModel.serverSet.map((_server: LiferayServer) => {
                return _server.name;
            });
            const serverName: string = await LiferayServerUtils.getServerName(
                liferayInstallPath,
                this._liferayModel.extensionStoragePath,
                existingServerNames,
            );

            serverStoragePath = await VscodeUtils.getServerStoragePath(
                this._liferayModel.extensionStoragePath,
                serverName,
            );

            const _server: LiferayServer | undefined = await LiferayServer.addServer(
                liferayInstallPath,
                serverStoragePath,
                serverName,
                bundleType,
            );
            _server && this._liferayModel.addServer(_server);
        } catch (error: any) {
            if (error instanceof Error) {
                window.showWarningMessage(StringConstants.ERROR_CREATE_SERVER);
                serverStoragePath && (await remove(serverStoragePath));
                console.error(error.message);
            }
        }

        return;
    }

    async browseServer(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);

        if (server) {
            switch (server.state) {
                case ServerState.STARTED:
                    await server.browseServer();
                    break;
                case ServerState.STOPPED:
                    const confirmation: string | undefined = await window.showWarningMessage(
                        StringConstants.WARNING_BROWSE_IDLE,
                        StringConstants.YES,
                        StringConstants.CANCEL,
                    );
                    confirmation && confirmation === StringConstants.YES && this.startServer(server);
                    break;
            }
        }
    }

    async deleteServer(_server: LiferayServer) {
        const server: LiferayServer | undefined = await this.#precheck(_server);
        if (server) {
            if (server.started) {
                const confirmation: string | undefined = await window.showWarningMessage(
                    StringConstants.DELETE_WARNING_IF_RUNNING,
                    StringConstants.YES,
                    StringConstants.NO,
                );
                if (confirmation && confirmation !== StringConstants.YES) {
                    return;
                }
                await this.stopServer(server);
            }
            this._liferayModel.deleteServer(server) && server.disposeOutputChannel();
        }
    }

    async deployJar(_server: LiferayServer) {
        const server: LiferayServer | undefined = await this.#precheck(_server);
        if (server) {
            try {
                const source: string = await this.#selectJar();
                const filename: string = source.replace(/^.*[\\\/]/, '');
                const destination: string = join(server.serverInstallPath, DEPLOY_DIR, filename);
                copyFile(source, destination, (err: Error) => {
                    throw err;
                });
            } catch (error: any) {
                if (error instanceof Error) {
                    window.showErrorMessage(StringConstants.ERROR_CREATE_SERVER);
                    console.error(error.message);
                }
            }
        }
    }

    dispose(): void {
        this._liferayModel.serverSet.forEach((_server: LiferayServer) => {
            _server.disposeOutputChannel();
        });
        this._liferayModel.saveServerListSync();
    }

    async openLifearyHome(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);
        server && commands.executeCommand(Commands.VSCODE_REVEAL_FILE_IN_OS, Uri.file(server.serverInstallPath));
    }

    async openServerConfiguration(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);

        if (server) {
            try {
                await VscodeUtils.openFile(server.serverConfigPath);
            } catch (error: any) {
                if (error instanceof Error) {
                    console.error(error.message);
                }
            }
        }
    }

    async renameServer(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);
        const VALID_NAME_REGEX: RegExp = /^[\w.-]+$/;

        if (server) {
            const newName: string | undefined = await window.showInputBox({
                placeHolder: StringConstants.getRenameInputPlaceholder(server.name),
                prompt: StringConstants.INPUT_NEW_LIFERAY_SERVER_NAME,
                value: server.name,
                validateInput: (name: string): string | undefined => {
                    if (name && !name.match(VALID_NAME_REGEX)) {
                        return StringConstants.ERROR_INPUT_VALID_SERVERNAME;
                    } else if (name && this._liferayModel.getLiferayServer(name)) {
                        return StringConstants.ERROR_NAME_ALREADY_TAKEN;
                    }
                    return undefined;
                },
            });
            if (newName) {
                server.name = newName;
                await this._liferayModel.saveServerList();
            }
        }
    }

    async setJavaRuntime(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server, true);
        if (server) {
            const runtime: string | undefined = await window.showInputBox({
                placeHolder: `Enter Java Runtime path`,
                prompt: `Enter Java runtime path for ${server.name}`,
                value: server.javaRuntime,
                validateInput: (path: string): string | undefined => {
                    if (path && path.trim() === '') {
                        return undefined;
                    }
                    if (path && !pathExistsSync(join(path, JAVA_EXECUTABLE_PATH))) {
                        return 'Invalid Java runtime path.';
                    }
                    return undefined;
                },
            });

            if (runtime !== undefined) {
                server.javaRuntime = runtime;
                await this._liferayModel.saveServerList();
            }
        }
    }

    async startServer(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server, true);
        if (server) {
            if (!(await server.startServer(this.#operatingSystem))) {
                return;
            }
        }
    }

    async stopServer(_server: LiferayServer, _force: boolean = false): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);

        let javaProcessIds: number[] = [];
        if (server) {
            if (_force && this.#operatingSystem === WINDOWS) {
                javaProcessIds = await NetworkUtils.getJavaProcessIDonWindows(_server.tomcatPath);
            } else if (!server.started && !_force) {
                window.showInformationMessage(StringConstants.LIFERAY_SERVER_STOPPED);
                return;
            }

            server.stopServer(this.#operatingSystem, _force, javaProcessIds);
        }
    }

    async toggleDeveloperMode(_server: LiferayServer): Promise<void> {
        const server: LiferayServer | undefined = await this.#precheck(_server);

        if (server) {
            await server.toggleDeveloperMode();
            await this._liferayModel.saveServerList();
        }
    }

    async #precheck(_server: LiferayServer, _createIfNone: boolean = false): Promise<LiferayServer | undefined> {
        if (CommonUtils.isArrayEmpty(this._liferayModel.serverSet) && !_createIfNone) {
            window.showInformationMessage(StringConstants.LIFERAY_SERVER_NONE_SELECTED);
            return;
        }
        return _server ?? (await this.#selectServer(_createIfNone));
    }

    async #selectBundleTypes(): Promise<string> {
        let bundleTypes: string[] = Object.values(LiferayBundleType);
        const bundleType: string | undefined = await window.showQuickPick(bundleTypes, {
            placeHolder: StringConstants.SELECT_BUNDLE_TYPE,
        });

        if (bundleType && bundleTypes.indexOf(bundleType) > -1) {
            return bundleType;
        }

        if (!bundleType) {
            throw Error(StringConstants.ERROR_SELECT_BUNDLE_TYPE);
        }
        throw Error(StringConstants.ERROR_INVALID_BUNDLE_TYPE);
    }

    async #selectLiferayInsatllPath(): Promise<string> {
        const workspaceFolders = workspace.workspaceFolders;
        const defaultUri =
            workspaceFolders && workspaceFolders[0].uri && workspaceFolders[0].uri.fsPath
                ? Uri.file(workspaceFolders[0].uri.fsPath)
                : undefined;

        const selectedPath: Uri[] | undefined = await window.showOpenDialog({
            defaultUri,
            canSelectFiles: false,
            canSelectFolders: true,
            openLabel: StringConstants.SELECT_LIFERAY_SERVER_DIR,
        });

        if (selectedPath && selectedPath[0]?.fsPath) {
            return selectedPath[0].fsPath;
        }

        throw Error(StringConstants.ERROR_INVALID_PATH_SELECTED);
    }

    async #selectJar(): Promise<string> {
        const workspaceFolders = workspace.workspaceFolders;
        const defaultUri =
            workspaceFolders && workspaceFolders[0].uri && workspaceFolders[0].uri.fsPath
                ? Uri.file(workspaceFolders[0].uri.fsPath)
                : undefined;

        const selectedPath: Uri[] | undefined = await window.showOpenDialog({
            defaultUri,
            canSelectFiles: true,
            canSelectFolders: false,
            openLabel: StringConstants.DEPLOY_JAR,
            filters: {
                'Java Archive file': ['jar'],
            },
        });

        if (selectedPath && selectedPath[0]?.fsPath) {
            return selectedPath[0].fsPath;
        }

        throw Error(StringConstants.ERROR_INVALID_PATH_SELECTED);
    }

    async #selectServer(_createIfNone: boolean = false): Promise<LiferayServer | undefined> {
        let items: QuickPickItem[] = this._liferayModel.serverSet;
        if (CommonUtils.isArrayEmpty(items) && !_createIfNone) {
            return undefined;
        }
        if (items.length === 1) {
            return <LiferayServer>items[0];
        }
        items = _createIfNone
            ? items.concat({ label: `$(plus) ` + StringConstants.LIFERAY_ADD_NEW_LIFERAY_SERVER, description: '' })
            : items;
        const pick: QuickPickItem | undefined = await window.showQuickPick(items, {
            placeHolder:
                _createIfNone && items.length === 1
                    ? StringConstants.LIFERAY_ADD_NEW_LIFERAY_SERVER
                    : StringConstants.SELECT_LIFERAY_SERVER_DIR,
        });

        if (pick) {
            if (pick instanceof LiferayServer) {
                return pick;
            } else {
                await this.addServer();
            }
        }

        return undefined;
    }
}
