import { existsSync, outputJson, outputJsonSync, readJsonSync, remove } from 'fs-extra';
import { join } from 'path';
import { commands } from 'vscode';
import { Commands } from '../Constants/Commands';
import { SERVER_JSON } from '../Constants/Constants';
import { CommonUtils } from '../Utils/CommonUtils';
import { LiferayServer } from './LiferayServer';

export class LiferayServerModel {
    #serverList: LiferayServer[] = [];
    #serversJsonFile: string;

    constructor(private _extensionStoragePath: string) {
        this.#serversJsonFile = join(_extensionStoragePath, SERVER_JSON);
        this.#initServerListSync();
    }

    get extensionStoragePath(): string {
        return this._extensionStoragePath;
    }

    get serverSet(): LiferayServer[] {
        return this.#serverList;
    }

    addServer(_server: LiferayServer): void {
        const index: number = this.#serverList.findIndex((item: LiferayServer) => item.name === _server.name);
        if (index > -1) {
            this.#serverList.splice(index, 1);
        }
        this.#serverList.push(_server);
        this.saveServerList();
    }

    deleteServer(_server: LiferayServer): boolean {
        const index: number = this.#serverList.findIndex((item: LiferayServer) => item.name === _server.name);
        if (index > -1) {
            const oldServer: LiferayServer[] = this.#serverList.splice(index, 1);
            if (!CommonUtils.isArrayEmpty(oldServer)) {
                remove(_server.extensionStoragePath);
                this.saveServerList();
                return true;
            }
        }

        return false;
    }

    getLiferayServer(_serverName: string): LiferayServer | undefined {
        return this.#serverList.find((item: LiferayServer) => item.name === _serverName);
    }

    async saveServerList(): Promise<void> {
        try {
            await outputJson(
                this.#serversJsonFile,
                this.#serverList.map((_server: LiferayServer) => {
                    return {
                        _name: _server.name,
                        _serverInstallPath: _server.serverInstallPath,
                        _storagePath: _server.extensionStoragePath,
                        _developerMode: _server.developerMode,
                        _bundleType: _server.bundleType,
                        _javaRuntime: _server.javaRuntime,
                    };
                }),
            );
            commands.executeCommand(Commands.LIFERAY_TREE_REFRESH);
        } catch (err: any) {
            console.error(err.toString());
        }
    }

    saveServerListSync(): void {
        try {
            outputJsonSync(
                this.#serversJsonFile,
                this.#serverList.map((_server: LiferayServer) => {
                    return {
                        _name: _server.name,
                        _serverInstallPath: _server.serverInstallPath,
                        _storagePath: _server.extensionStoragePath,
                        _developerMode: _server.developerMode,
                        _bundleType: _server.bundleType,
                        _javaRuntime: _server.javaRuntime,
                    };
                }),
            );
        } catch (error: any) {
            console.error(error);
        }
    }

    #initServerListSync(): void {
        type ServerJsonObject = {
            _name: string;
            _serverInstallPath: string;
            _storagePath: string;
            _developerMode: boolean;
            _bundleType: string;
            _javaRuntime: string;
        };
        try {
            if (existsSync(this.#serversJsonFile)) {
                const jsonObjectArray = readJsonSync(this.#serversJsonFile) as ServerJsonObject[];
                if (!CommonUtils.isArrayEmpty(jsonObjectArray)) {
                    const serverArray: LiferayServer[] = [];
                    jsonObjectArray.forEach(obj => {
                        try {
                            const server = new LiferayServer(
                                obj._name,
                                obj._serverInstallPath,
                                obj._storagePath,
                                obj._developerMode,
                                obj._bundleType,
                                obj._javaRuntime,
                            );
                            serverArray.push(server);
                        } catch (error) {
                            if (error instanceof Error) {
                                console.error(error.message);
                            }
                        }
                    });
                    this.#serverList = this.#serverList.concat(...serverArray);
                }
            }
        } catch (error: any) {
            console.error(error);
        }
    }
}
