import { ensureFileSync, readdir, readFileSync, writeFileSync } from 'fs-extra';

export class FileUtils {
    static async getAllDirectories(path: string): Promise<string[]> {
        return (await readdir(path, { withFileTypes: true })).filter(dir => dir.isDirectory()).map(dir => dir.name);
    }

    static readFileAsArraySync(path: string): string[] {
        ensureFileSync(path);
        const lines: string[] = readFileSync(path).toString().split('\n');
        lines.map(_ => _.trim());
        return lines;
    }

    static writeFileFromArraySync(path: string, lines: string[]): void {
        ensureFileSync(path);
        writeFileSync(path, lines.join('\n'));
    }
}
