import { ChildProcess, spawn, SpawnOptions } from 'child_process';
import { pathExists, readFileSync } from 'fs-extra';
import { basename, join } from 'path';
import { OutputChannel, window } from 'vscode';
import {
    BIN_DIR,
    CATALINA_SCRIPT,
    CORE_DIR,
    DARWIN,
    DEPLOY_DIR,
    EXT_SERVER_STORE_DIR,
    LIFERAY_SHUTDOWN_SCRIPT,
    LIFERAY_STARTUP_SCRIPT,
    LINUX,
    OSGI_DIR,
    PORTAL_BOOTSTRAP_JAR,
    WINDOWS,
} from '../Constants/Constants';
import { LiferayBundleType, LiferayEvent, LiferayMajorVersion, PortKind } from '../Constants/Enums';
import { StringConstants } from '../Constants/StringConstants';
import { LiferayServer } from '../Liferay/LiferayServer';
import { FileUtils } from './FileUtils';
import { TomcatUtils } from './TomcatUtils';
import { VscodeUtils } from './VscodeUtils';
import find = require('find-process');

export class LiferayServerUtils {
    static async getServerName(
        serverInstallPath: string,
        extenstionStoragePath: string,
        existingServerNames: string[],
    ): Promise<string> {
        const workspace: string = await VscodeUtils.getExtensionWorkspaceStoragePath(
            extenstionStoragePath,
            EXT_SERVER_STORE_DIR,
        );
        const directories: string[] = await FileUtils.getAllDirectories(workspace);
        let serverName: string = basename(serverInstallPath);

        let index: number = 1;
        while (directories.indexOf(serverName) >= 0 || existingServerNames.indexOf(serverName) >= 0) {
            serverName = basename(serverInstallPath).concat(`-${index}`);
            index += 1;
        }
        return serverName;
    }

    static async isValidLiferayBundle(
        serverInstallPath: string,
        bundleType: LiferayBundleType = LiferayBundleType.TOMCAT,
        _bundleMajorVersion?: LiferayMajorVersion,
    ): Promise<boolean> {
        let isValidTomcatInstallation: Promise<boolean>;
        let isValidLiferayBundle: Promise<boolean>;
        isValidLiferayBundle = this.#_isValidLiferayBundle(serverInstallPath);

        if (bundleType === LiferayBundleType.TOMCAT) {
            const tomcatRootPath = await TomcatUtils.getTomcatDirectoryPath(serverInstallPath);
            if (!tomcatRootPath) {
                return false;
            }
            isValidTomcatInstallation = TomcatUtils.isValidTomcatInstallation(tomcatRootPath);

            return (await isValidLiferayBundle) && (await isValidTomcatInstallation);
        }

        return false;
    }

    static async #_isValidLiferayBundle(serverInstallPath: string): Promise<boolean> {
        const deployFolderExists: Promise<boolean> = pathExists(join(serverInstallPath, DEPLOY_DIR));
        const portalBootstrapJarExists: Promise<boolean> = pathExists(
            join(serverInstallPath, OSGI_DIR, CORE_DIR, PORTAL_BOOTSTRAP_JAR),
        );

        return (await deployFolderExists) && (await portalBootstrapJarExists);
    }

    static async executeLiferayTomcatBundleCommand(
        _event: LiferayEvent,
        _operatingSystem: string,
        _args: string | undefined,
        _server: LiferayServer,
        _javaPath: string,
    ): Promise<void> {
        if (_operatingSystem === LINUX || _operatingSystem === DARWIN) {
            let passPidFile: boolean = true;
            if (_event === LiferayEvent.STOP) {
                passPidFile = false;
            }
            await LiferayServerUtils.#executeLiferayTomcatScript(
                _server.outputChannel,
                _event,
                _args,
                _server.tomcatPath,
                _javaPath,
                passPidFile ? _server.pidFile : undefined,
            );
        } else if (_operatingSystem === WINDOWS) {
            const jvmOptions: string[] = _server.getJVMOptions();
            if (_event === LiferayEvent.START) {
                jvmOptions.push('start');
            } else if (_event === LiferayEvent.STOP) {
                jvmOptions.push('stop');
            }

            _server.outputChannel.appendLine(`Using JVM Options: ${jvmOptions.join(' ')}`);
            LiferayServerUtils.#executeCMD(
                _server.outputChannel,
                this.name,
                _javaPath,
                {
                    shell: true,
                },
                ...jvmOptions,
            );
        }
    }

    static async #executeLiferayTomcatScript(
        _outputChannel: OutputChannel,
        _event: LiferayEvent,
        _argument: string | undefined,
        _tomcatPath: string,
        _javaPath: string,
        _pidFile?: string | undefined,
    ) {
        let command: string = join(_tomcatPath, BIN_DIR);
        let arg: string[] = new Array<string>();
        switch (_event) {
            case LiferayEvent.START:
                command = join(command, LIFERAY_STARTUP_SCRIPT);
                arg.push(command);
                break;
            case LiferayEvent.STOP:
                command = join(command, LIFERAY_SHUTDOWN_SCRIPT);
                arg.push(command);
                break;
            case LiferayEvent.FORCE_STOP:
                command = join(command, CATALINA_SCRIPT);
                arg.push(command);
                arg.push('stop');
                arg.push(_argument!);
                break;
            default:
                break;
        }

        if (_pidFile) {
            _outputChannel.appendLine(`Using CATALINA_PID: ${_pidFile}`);
        }

        const pid = await spawn('bash', arg, {
            shell: true,
            env: {
                ...process.env,
                _RUNJAVA: _javaPath,
                CATALINA_PID: _pidFile ?? '',
            },
        });

        pid.stdout.on('data', data => {
            _outputChannel.show();
            _outputChannel.appendLine(`${data}`);
        });

        pid.stderr.on('data', data => {
            _outputChannel.appendLine(`${data}`);
        });
    }

    static async checkTomcatProcessByPid(
        pidFile: string,
        outputChannel: OutputChannel,
        suppressLogs = false,
    ): Promise<boolean> {
        const pid: string = readFileSync(pidFile).toString();
        if (pid.trim() === '') {
            return false;
        }

        try {
            const process: {
                pid: number;
                ppid?: number | undefined;
                uid?: number | undefined;
                gid?: number | undefined;
                name: string;
                cmd: string;
            }[] = await find('pid', Number.parseInt(pid));
            if (process.length > 0) {
                if (!suppressLogs) {
                    outputChannel.show();
                    outputChannel.appendLine(`Tomcat appears to still be running with PID ${pid}`);
                    outputChannel.appendLine(
                        'If the following process is not a Tomcat process, remove the PID file and try again',
                    );
                }
                return true;
            }
        } catch (error: any) {
            console.log("Error checking pid: " + error.message);
        }
        
        return false;
    }

    static async #executeCMD(
        _outputChannel: OutputChannel,
        _serverName: string,
        _javaPath: string,
        _options: SpawnOptions,
        ..._args: string[]
    ): Promise<void> {
        await new Promise((resolve: (_?: unknown) => void, reject: (e: Error) => void): void => {
            _outputChannel.show();
            let stderr: string = '';
            const pid: ChildProcess = spawn(`"${_javaPath}"`, _args, _options);

            if (pid.stdout) {
                pid.stdout.on('data', (data: string | Buffer): void =>
                    _outputChannel.append(_serverName ? `${data.toString()}` : data.toString()),
                );
            }
            if (pid.stderr) {
                pid.stderr.on('data', (data: string | Buffer) => {
                    stderr = stderr.concat(data.toString());
                    _outputChannel.append(_serverName ? `${data.toString()}` : data.toString());
                });
            }
            pid.on('error', (err: Error) => {
                reject(err);
            });
            pid.on('exit', (code: number) => {
                if (code !== 0) {
                    reject(new Error(`Command failed with exit code ${code}`));
                }
                resolve();
            });
        });
    }

    static async getLiferayPorts(
        _serverName: string,
        _serverConfigPath: string,
        _bundleType: LiferayBundleType,
    ): Promise<(string | undefined)[] | undefined> {
        let httpPort: string | undefined;
        let httpsPort: string | undefined;
        let serverPort: string | undefined;
        if (_bundleType === LiferayBundleType.TOMCAT) {
            const serverConfig: string = _serverConfigPath;
            httpPort = await TomcatUtils.getPort(serverConfig, PortKind.HTTP);
            if (!httpPort) {
                window.showErrorMessage(StringConstants.getPortMissing(PortKind.HTTP, _serverName));
                return;
            }
            httpsPort = await TomcatUtils.getPort(serverConfig, PortKind.HTTPS);
            serverPort = await TomcatUtils.getPort(serverConfig, PortKind.SERVER);
            if (!serverPort) {
                window.showErrorMessage(StringConstants.getPortMissing(PortKind.SERVER, _serverName));
                return;
            }
        }
        return [httpPort, httpsPort, serverPort];
    }
}
