import { parseString } from 'xml2js';

export class CommonUtils {
    static isArrayEmpty<T>(array: T[]): boolean {
        return array.length === 0 ? true : false;
    }

    static async parseXml(xml: string): Promise<any> {
        return new Promise((resolve: (obj: {}) => void, reject: (e: Error) => void): void => {
            parseString(xml, { explicitArray: true }, (err: Error, res: {}) => {
                if (err) {
                    return reject(err);
                }
                return resolve(res);
            });
        });
    }

    static removeWhitespaces(str: string): string {
        const REGEX: RegExp = /\s+/g;
        return str.replace(REGEX, '');
    }
}
