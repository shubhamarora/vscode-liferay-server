import find = require('find-process');
import { check } from 'tcp-port-used';
import { commands, window } from 'vscode';
import { Commands } from '../Constants/Commands';
import { LOCALHOST_IP } from '../Constants/Constants';
import { PortKind } from '../Constants/Enums';
import { StringConstants } from '../Constants/StringConstants';
import { LiferayServer } from '../Liferay/LiferayServer';

type Port = {
    port: string | undefined;
    portKind: PortKind;
};
export class NetworkUtils {
    static async verifyPorts(ports: Port[], server: LiferayServer): Promise<boolean> {
        for (const item of ports) {
            try {
                if (item.port) {
                    let inUse = await check(Number.parseInt(item.port), LOCALHOST_IP);
                    if (inUse) {
                        console.debug(StringConstants.portInUse(item.port, item.portKind, server.name));
                        const confirmation: string | undefined = await window.showErrorMessage(
                            StringConstants.portInUse(item.port, item.portKind, server.name),
                            StringConstants.OPEN_SERVER_XML,
                        );
                        if (confirmation && confirmation === StringConstants.OPEN_SERVER_XML) {
                            commands.executeCommand(Commands.LIFERAY_SERVER_CONFIGURATION, server);
                        }

                        return true;
                    }
                }
            } catch (error: unknown) {
                if (error instanceof Error) {
                    console.error('Error on port check: ', error.message);
                }
                console.error('Error on port check: ', error);
            }
        }
        return false;
    }

    static async getJavaProcessIDonWindows(_tomcatPath: string): Promise<number[]> {
        const pidArray: number[] = [];
        const processList = await find('name', 'java', true);
        console.log(processList);
        processList.forEach(item => {
            if (item.name === 'java.exe') {
                if (item.cmd.includes(_tomcatPath)) {
                    pidArray.push(item.pid);
                }
            }
        });
        return pidArray;
    }
}
