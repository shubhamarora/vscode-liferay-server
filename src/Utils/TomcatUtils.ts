import { ensureDirSync, ensureFile, pathExists, readFile, writeFile } from 'fs-extra';
import { delimiter, join } from 'path';
import { Builder } from 'xml2js';
import {
    BIN_DIR,
    BOOTSTRAP_FILE,
    BOOTSTRAP_JAR,
    CATALINA,
    CATALINA_BASE_KEY,
    CATALINA_HOME_KEY,
    CLASSPATH_KEY,
    CONF_DIR,
    DEFAULT_XMX_KEY,
    ENABLE_CLEAR_REFERENCES,
    ENCODING_KEY,
    EPHEMERAL_DH_KEY_SIZE_KEY,
    HTTP,
    JAVA_PROTOCOL_HANDLER_PKGS_KEY,
    LOGGING_CONFIG_FILE_KEY,
    LOGGING_MANAGER_KEY,
    LOGGING_PROPERTIES,
    MAX_METASPACE_SIZE_KEY,
    MAX_NEW_SIZE_KEY,
    NEW_SIZE_KEY,
    PREFER_IPV4_KEY,
    SECURITY_LISTENER_KEY,
    SERVER_XML,
    SURVIVOR_RATIO_KEY,
    TEMP_DIR,
    TIMEZONE_KEY,
    TMP_DIR_KEY,
    TOMCAT_JULI_JAR,
} from '../Constants/Constants';
import { PortKind } from '../Constants/Enums';
import { CommonUtils } from './CommonUtils';
import { FileUtils } from './FileUtils';

export class TomcatUtils {
    static async getTomcatDirectoryName(path: string): Promise<string | undefined> {
        const directories = await FileUtils.getAllDirectories(path);
        const tomcatDirectory: string[] = directories.filter(dir => dir.startsWith('tomcat'));
        return tomcatDirectory[0] ?? undefined;
    }

    static async getTomcatDirectoryPath(installPath: string): Promise<string | undefined> {
        const tomcatDirectoryName = await this.getTomcatDirectoryName(installPath);
        if (!tomcatDirectoryName) {
            return undefined;
        }

        return join(installPath, tomcatDirectoryName);
    }

    static async isValidTomcatInstallation(tomcatInstallPath: string): Promise<boolean> {
        const SERVER_XML_PATH: string = join(tomcatInstallPath, CONF_DIR, SERVER_XML);
        const WEB_XML_PATH: string = join(tomcatInstallPath, CONF_DIR, SERVER_XML);
        const configFileExists: Promise<boolean> = pathExists(SERVER_XML_PATH);
        const serverWebFileExists: Promise<boolean> = pathExists(WEB_XML_PATH);
        const serverBootstrapJarFileExists: Promise<boolean> = pathExists(
            join(tomcatInstallPath, BIN_DIR, BOOTSTRAP_JAR),
        );
        const serverJuliJarFileExists: Promise<boolean> = pathExists(join(tomcatInstallPath, BIN_DIR, TOMCAT_JULI_JAR));

        return (
            (await configFileExists) &&
            (await serverWebFileExists) &&
            (await serverBootstrapJarFileExists) &&
            (await serverJuliJarFileExists)
        );
    }

    static async getPort(serverXml: string, kind: PortKind): Promise<string | undefined> {
        if (!(await pathExists(serverXml))) {
            throw new Error('There are no Liferay Servers.');
        }
        const xml: string = await readFile(serverXml, 'utf8');
        let port: string | undefined;
        try {
            const jsonObj: any = await CommonUtils.parseXml(xml);
            if (kind === PortKind.SERVER) {
                port = jsonObj.Server.$.port;
            } else if (kind === PortKind.HTTP) {
                port = jsonObj.Server.Service.find((item: any) => item.$.name === CATALINA).Connector.find(
                    (item: any) => item.$.protocol === undefined || item.$.protocol.startsWith(HTTP),
                ).$.port;
            } else if (kind === PortKind.HTTPS) {
                port = jsonObj.Server.Service.find((item: any) => item.$.name === CATALINA).Connector.find(
                    (item: any) => item.$.SSLEnabled.toLowerCase() === 'true',
                ).$.port;
            }
        } catch (err) {
            port = undefined;
        }
        return port;
    }

    static async copyServerConfig(source: string, target: string): Promise<void> {
        const xml: string = await readFile(source, 'utf8');
        const jsonObj: {} = await CommonUtils.parseXml(xml);
        const builder: Builder = new Builder();
        const newXml: string = builder.buildObject(jsonObj);
        await ensureFile(target);
        await writeFile(target, newXml);
    }

    static async setPort(serverXml: string, kind: PortKind, value: string): Promise<void> {
        if (!(await pathExists(serverXml))) {
            throw new Error('There are no Liferay Servers.');
        }
        const xml: string = await readFile(serverXml, 'utf8');
        const jsonObj: any = await CommonUtils.parseXml(xml);
        if (kind === PortKind.SERVER) {
            jsonObj.Server.$.port = value;
        } else {
            const catalinaService: any = jsonObj.Server.Service.find((item: any) => item.$.name === CATALINA);

            if (kind === PortKind.HTTP) {
                const httpConnector: any = catalinaService.Connector.find(
                    (item: any) => !item.$.protocol || item.$.protocol.startsWith(HTTP),
                );
                httpConnector.$.port = value;
            } else if (kind === PortKind.HTTPS) {
                const httpsConnector: any = catalinaService.Connector.find(
                    (item: any) => item.$.SSLEnabled.toLowerCase() === 'true',
                );
                httpsConnector.$.port = value;
            }
        }
        const builder: Builder = new Builder();
        const newXml: string = builder.buildObject(jsonObj);
        await writeFile(serverXml, newXml);
    }

    static getDefaultJVMOptions(tomcatPath: string): string[] {
        const default_jvm_options: string[] = [];
        const bootstrapJarPath: string = `"${join(tomcatPath!, BIN_DIR, BOOTSTRAP_JAR)}"`;
        const tomcatJuliJarPath: string = `"${join(tomcatPath!, BIN_DIR, TOMCAT_JULI_JAR)}"`;
        const tempDirectoryPath: string = join(tomcatPath!, TEMP_DIR);
        const loggingPropertiesPath: string = join(tomcatPath!, CONF_DIR, LOGGING_PROPERTIES);
        const classPath = [bootstrapJarPath, tomcatJuliJarPath].join(delimiter);
        ensureDirSync(tempDirectoryPath);

        default_jvm_options.push(`${CLASSPATH_KEY} ${classPath}`);
        default_jvm_options.push(`${CATALINA_BASE_KEY}="${tomcatPath!}"`);
        default_jvm_options.push(`${CATALINA_HOME_KEY}="${tomcatPath!}"`);
        default_jvm_options.push(EPHEMERAL_DH_KEY_SIZE_KEY);
        default_jvm_options.push(JAVA_PROTOCOL_HANDLER_PKGS_KEY);
        default_jvm_options.push(SECURITY_LISTENER_KEY);
        default_jvm_options.push(ENCODING_KEY);
        default_jvm_options.push(`${TMP_DIR_KEY}="${tempDirectoryPath}"`);
        default_jvm_options.push(PREFER_IPV4_KEY);
        default_jvm_options.push(`${LOGGING_CONFIG_FILE_KEY}="${loggingPropertiesPath}"`);
        default_jvm_options.push(LOGGING_MANAGER_KEY);
        default_jvm_options.push(ENABLE_CLEAR_REFERENCES);
        default_jvm_options.push(TIMEZONE_KEY);
        default_jvm_options.push(DEFAULT_XMX_KEY);
        default_jvm_options.push(MAX_NEW_SIZE_KEY);
        default_jvm_options.push(MAX_METASPACE_SIZE_KEY);
        default_jvm_options.push(NEW_SIZE_KEY);
        default_jvm_options.push(SURVIVOR_RATIO_KEY);
        default_jvm_options.push(BOOTSTRAP_FILE, '"$@"');

        return default_jvm_options;
    }
}
