import { pathExistsSync } from 'fs-extra';
import { env } from 'process';
import { OutputChannel, workspace, WorkspaceConfiguration } from 'vscode';
import { JAVA_EXECUTABLE_PATH, JAVA_HOME } from '../Constants/Constants';

type RuntimeConfig = {
    name: string;
    path: string;
    default?: boolean;
};

export class JavaUtils {
    static getJavaExecutable(outputChannel: OutputChannel): string | undefined {
        // JAVA_HOME
        let javaPath: string | undefined = this.#getGlobalJavaHome();
        if (javaPath) {
            outputChannel.appendLine(`JAVA_HOME = ${javaPath}`);
        }
        // User/Workspace Setting: java.home
        javaPath = this.#getVscodeJavaHome(javaPath);
        if (javaPath) {
            outputChannel.appendLine(`java.home = ${javaPath}`);
        }
        // User Setting: java.configuration.runtime
        // Set default: true
        javaPath = this.#getDefaultJavaRuntime(javaPath);
        if (javaPath) {
            outputChannel.appendLine(`java.configuration.runtimes [default] = ${javaPath}`);
        }

        if (javaPath) {
            outputChannel.appendLine(`Using JAVA Executable from: ${javaPath}`);
        } else {
            outputChannel.appendLine('Using JAVA from env.PATH');
        }

        javaPath = javaPath ? javaPath + JAVA_EXECUTABLE_PATH : undefined;
        javaPath && pathExistsSync(javaPath);

        return javaPath;
    }

    static #getDefaultJavaRuntime(_javaPath: string | undefined): string | undefined {
        const config: WorkspaceConfiguration = workspace.getConfiguration('java.configuration');
        let javaPath: string | undefined = undefined;

        if (config.has('runtimes')) {
            const javaRuntimeCongigs: RuntimeConfig[] | [] = config.get<RuntimeConfig[]>('runtimes') ?? [];
            javaRuntimeCongigs.forEach(config => {
                if (config.default) {
                    javaPath = config.path;
                }
            });
        }

        return javaPath || _javaPath;
    }

    static #getGlobalJavaHome(): string | undefined {
        const javaPath: string | undefined = env[JAVA_HOME] ?? undefined;
        return javaPath;
    }

    static #getVscodeJavaHome(_javaPath: string | undefined): string | undefined {
        const config: WorkspaceConfiguration = workspace.getConfiguration('java');
        let javaPath: string | undefined = undefined;
        if (config.has('home')) {
            javaPath = config.get<string>('home');
        }

        return javaPath || _javaPath;
    }
}
