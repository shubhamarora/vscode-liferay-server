import { ensureDir, pathExists } from 'fs-extra';
import { join } from 'path';
import { ConfigurationTarget, Uri, window, workspace, WorkspaceConfiguration } from 'vscode';
import { EXT_SERVER_STORE_DIR } from '../Constants/Constants';

export class VscodeUtils {
    static async disableDeveloperModeToggleMessage() {
        const config: WorkspaceConfiguration = workspace.getConfiguration('liferay.server');
        if (config.has('showDeveloperModeToggleMessage')) {
            try {
                await config.update('showDeveloperModeToggleMessage', false, ConfigurationTarget.Global);
            } catch (error: unknown) {
                if (error instanceof Error) {
                    console.error(error.message);
                    return;
                }
                console.error('Failed to update liferay.server.showDeveloperModeToggleMessage with error: ' + error);
            }
            return;
        }
        console.error('Failed to update liferay.server.showDeveloperModeToggleMessage. Property not found.');
        return;
    }

    static async getExtensionWorkspaceStoragePath(path: string, storeDirectory: string): Promise<string> {
        const dir: string = join(path, storeDirectory);
        await ensureDir(dir);
        return dir;
    }

    static async getServerStoragePath(path: string, serverName: string): Promise<string> {
        const dir: string = join(await this.getExtensionWorkspaceStoragePath(path, EXT_SERVER_STORE_DIR), serverName);
        await ensureDir(dir);
        return dir;
    }

    static async openFile(path: string): Promise<void> {
        if (!(await pathExists(path))) {
            window.showErrorMessage(`File ${path} does not exist.`);
            throw new Error(`File ${path} does not exist.`);
        }
        window.showTextDocument(Uri.file(path), { preview: false });
    }

    static shouldShowDeveloperModeInformationMessage(): boolean {
        const config: WorkspaceConfiguration = workspace.getConfiguration('liferay.server');
        if (config.has('showDeveloperModeToggleMessage')) {
            const shouldShow: boolean | undefined = config.get<boolean>('showDeveloperModeToggleMessage');
            if (shouldShow !== undefined) {
                return shouldShow;
            }
        }
        return true;
    }
}
