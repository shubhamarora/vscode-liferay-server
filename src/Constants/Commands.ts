export namespace Commands {
    // Global Pallete Commands
    export const LIFERAY_SERVER_ADD: string = 'liferay.server.add';
    export const LIFERAY_SERVER_RENAME: string = 'liferay.server.rename';
    export const LIFERAY_SERVER_DELETE: string = 'liferay.server.delete';
    export const LIFERAY_SERVER_START: string = 'liferay.server.start';
    export const LIFERAY_SERVER_STOP: string = 'liferay.server.stop';
    export const LIFERAY_SERVER_FORCE_STOP: string = 'liferay.server.forceStop';
    export const LIFERAY_SERVER_BROWSE: string = 'liferay.server.browse';
    export const LIFERAY_SERVER_CONFIGURATION: string = 'liferay.server.configuration';
    export const LIFERAY_SERVER_DEVELOPER_MODE: string = 'liferay.server.developerMode';
    export const LIFERAY_SERVER_OPEN_HOME_DIR: string = 'liferay.server.openLiferayHome';
    export const LIFERAY_SERVER_SET_JAVA_RUNTIME: string = 'liferay.server.setJavaRuntime';
    export const LIFERAY_SERVER_DEPLOY_JAR: string = 'liferay.server.deployJar';

    // Context Menu Commands
    export const LIFERAY_SERVER_RENAME_CONTEXT: string = 'liferay.server.rename.context';
    export const LIFERAY_SERVER_DELETE_CONTEXT: string = 'liferay.server.delete.context';
    export const LIFERAY_SERVER_START_CONTEXT: string = 'liferay.server.start.context';
    export const LIFERAY_SERVER_STOP_CONTEXT: string = 'liferay.server.stop.context';
    export const LIFERAY_SERVER_FORCE_STOP_CONTEXT: string = 'liferay.server.forceStop.context';
    export const LIFERAY_SERVER_BROWSE_CONTEXT: string = 'liferay.server.browse.context';
    export const LIFERAY_SERVER_CONFIGURATION_CONTEXT: string = 'liferay.server.configuration.context';
    export const LIFERAY_SERVER_DEVELOPER_MODE_CONTEXT: string = 'liferay.server.developerMode.context';
    export const LIFERAY_SERVER_OPEN_HOME_DIR_CONTEXT: string = 'liferay.server.openLiferayHome.context';
    export const LIFERAY_SERVER_SET_JAVA_RUNTIME_CONTEXT: string = 'liferay.server.setJavaRuntime.context';
    export const LIFERAY_SERVER_DEPLOY_JAR_CONTEXT: string = 'liferay.server.deployJar.context';

    // Tree View Commands
    export const LIFERAY_TREE_REFRESH: string = 'liferay.tree.refresh';

    // Vscode Internal Commands
    export const VSCODE_REVEAL_FILE_IN_OS: string = 'revealFileInOS';
}
