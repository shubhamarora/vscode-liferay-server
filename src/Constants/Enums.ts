export enum ServerState {
    STARTED = 'Started',
    STOPPED = 'Stopped',
    STARTING = 'Starting',
    STOPPING = 'Stopping',
}

export enum LiferayMajorVersion {
    LIFERAY_7_0 = '7.0',
    LIFERAY_7_1 = '7.1',
    LIFERAY_7_2 = '7.2',
    LIFERAY_7_3 = '7.3',
    LIFERAY_7_4 = '7.4',
}

export enum LiferayBundleType {
    TOMCAT = 'Tomcat',
}

export enum PortKind {
    SERVER = 'Server',
    HTTP = 'Http',
    HTTPS = 'Https',
}

export enum LiferayEvent {
    START = 'Start',
    STOP = 'Stop',
    FORCE_STOP = 'ForceStop',
    RESTART = 'Restart',
    DEBUG = 'Debug',
}
