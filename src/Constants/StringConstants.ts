import { PortKind } from './Enums';

export namespace StringConstants {
    export const YES: string = 'Yes';
    export const NO: string = 'No';
    export const CANCEL: string = 'Cancel';
    export const OK: string = 'Ok';
    export const DO_NOT_SHOW_AGAIN: string = "Don't show again";
    export const OPEN_SERVER_XML: string = 'Open server.xml';

    export const ENABLED: string = 'Enabled';
    export const DISABLED: string = 'Disabled';

    export const ERROR_INVALID_BUNDLE_TYPE: string = 'Invalid Liferay bundle type selected.';
    export const ERROR_INVALID_PATH_SELECTED: string = 'Invalid Liferay install path.';
    export const ERROR_SELECT_BUNDLE_TYPE: string = 'Select Liferay bundle type.';
    export const ERROR_INVALID_LIFERAY_BUNDLE: string = 'Invalid Liferay bundle selected.';
    export const ERROR_CREATE_SERVER: string = 'Error while adding Liferay server.';
    export const ERROR_START_SERVER: string = 'Error while Starting Liferay server.';
    export const ERROR_DEPLOY: string = 'Error deploying jar to Liferay server.';
    export const ERROR_FORCE_STOP: string = 'Error force stopping Liferay server.';

    export const SELECT_LIFERAY_SERVER_DIR: string = 'Select Liferay server directory';
    export const ENSURE_VALID_LIFERAY_SERVER_DIR: string =
        'Please make sure you select a valid Liferay bundle directory';
    export const INPUT_NEW_LIFERAY_SERVER_NAME: string = 'Input a new Liferay server name';
    export const ERROR_INPUT_VALID_SERVERNAME: string = 'Please input a valid Liferay server name';
    export const ERROR_NAME_ALREADY_TAKEN: string = 'The liferay server name was already taken, please re-input';
    export const ERROR_JAVA_HOME: string =
        'JAVA Executable not found. Set JAVA_HOME or java.home or default config in java.configuration.runtimes';

    export const DELETE_WARNING_IF_RUNNING: string =
        'This Liferay server is already running, are you sure you want to delete it?';
    export const WARNING_BROWSE_IDLE: string =
        'The Liferay server needs to be started before browsing. Would you like to start it now?';
    export const LIFERAY_SERVER_STOPPED: string = 'This Liferay server is already stopped.';
    export const LIFERAY_SERVER_STARTED: string = 'This Liferay server is already started.';
    export const LIFERAY_SERVER_NONE_SELECTED: string = 'There are no Liferay Servers selected.';

    export const LIFERAY_ADD_NEW_LIFERAY_SERVER: string = 'Add new server';
    export const LIFERAY_SELECT_LIFERAY_SERVER: string = 'Select Liferay server';

    export const SELECT_BUNDLE_TYPE: string = 'Select Liferay bundle type';
    export const INVALID_BUNDLE_TYPE: string = 'Invalid Liferay bundle type selected.';

    export const DEPLOY_JAR: string = 'Deploy Jar';

    export const ENABLED_DEVELOPER_MODE: string = 'Liferay server developer mode is enabled.';
    export const DISABLED_DEVELOPER_MODE: string = 'Liferay server developer mode is disabled.';

    export const FORCE_STOP_NOT_SUPPORTED: string = 'Force Stop not supported on windows.';

    export function toggleDeveloperModeMessage(type: string, serverName: string): string {
        return `Liferay server developer mode is ${type.toLowerCase()} for ${serverName}`;
    }

    export function getRenameInputPlaceholder(serverName: string): string {
        return `Enter a new server name for ${serverName}`;
    }

    export function portInUse(port: string, portKind: PortKind, name: string): string {
        return `${portKind} Port ${port} already in use. Unable to start Liferay server ${name}.`;
    }

    export function getPortMissing(portKind: PortKind, name: string): string {
        return `${portKind} Port missing for Liferay server ${name}. Unable to start the server.`;
    }
}
