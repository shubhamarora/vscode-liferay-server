// Directories
export const BIN_DIR: string = 'bin';
export const CONF_DIR: string = 'conf';
export const LOGS_DIR: string = 'logs';
export const OSGI_DIR: string = 'osgi';
export const CORE_DIR: string = 'core';
export const TEMP_DIR: string = 'temp';
export const DEPLOY_DIR: string = 'deploy';
export const RESOURCES_DIR: string = 'resources';
export const EXT_SERVER_STORE_DIR: string = 'liferay';

// Files
export const SERVER_XML: string = 'server.xml';
export const SERVER_JSON: string = 'servers.json';
export const PID_TXT: string = 'pid.txt';
export const CATALINA_OUT: string = 'catalina.out';
export const PORTEL_EXT_PROPERTIES: string = 'portal-ext.properties';
export const PORTAL_DEVELOPER_PROPERTIES: string = 'portal-developer.properties';
export const LOGGING_PROPERTIES: string = 'logging.properties';

// Files - Scripts
export const LIFERAY_STARTUP_SCRIPT: string = 'startup.sh';
export const LIFERAY_SHUTDOWN_SCRIPT: string = 'shutdown.sh';
export const CATALINA_SCRIPT: string = 'catalina.sh';

// Files - Jars
export const PORTAL_BOOTSTRAP_JAR: string = 'com.liferay.portal.bootstrap.jar';
export const BOOTSTRAP_JAR: string = 'bootstrap.jar';
export const TOMCAT_JULI_JAR: string = 'tomcat-juli.jar';

// Extensions
export const SVG_EXT: string = 'svg';

// Constants - OS 
export const WINDOWS: string = 'Windows_NT';
export const LINUX: string = 'Linux';
export const DARWIN: string = 'Darwin';

// Constants - Network
export const HTTP: string = 'HTTP/';
export const LOCALHOST: string = 'localhost';
export const LOCALHOST_IP: string = '127.0.0.1';

// Constants - Java
export const JAVA_HOME: string = 'JAVA_HOME';
export const JAVA_EXECUTABLE_NAME: string = 'java';
export const JAVA_EXECUTABLE_PATH: string = '/bin/' + JAVA_EXECUTABLE_NAME;

// Constants - JVM 
export const CLASSPATH_KEY: string = '-classpath';
export const CATALINA_BASE_KEY: string = '-Dcatalina.base';
export const CATALINA_HOME_KEY: string = '-Dcatalina.home';
export const EPHEMERAL_DH_KEY_SIZE_KEY: string = '-Djdk.tls.ephemeralDHKeySize=2048';
export const JAVA_PROTOCOL_HANDLER_PKGS_KEY: string = '-Djava.protocol.handler.pkgs=org.apache.catalina.webresources';
export const SECURITY_LISTENER_KEY: string = '-Dorg.apache.catalina.security.SecurityListener.UMASK=0027';
export const ENCODING_KEY: string = '-Dfile.encoding=UTF8';
export const TMP_DIR_KEY: string = '-Djava.io.tmpdir';
export const PREFER_IPV4_KEY: string = '-Djava.net.preferIPv4Stack=true';
export const LOGGING_CONFIG_FILE_KEY: string = '-Djava.util.logging.config.file';
export const LOGGING_MANAGER_KEY: string = '-Djava.util.logging.manager=org.apache.juli.ClassLoaderLogManager';
export const ENABLE_CLEAR_REFERENCES: string =
    '-Dorg.apache.catalina.loader.WebappClassLoader.ENABLE_CLEAR_REFERENCES=false';
export const TIMEZONE_KEY: string = '-Duser.timezone=GMT';
export const DEFAULT_XMX_KEY: string = '-Xmx2560m';
export const MAX_NEW_SIZE_KEY: string = '-XX:MaxNewSize=1536m';
export const MAX_METASPACE_SIZE_KEY: string = '-XX:MaxMetaspaceSize=768m';
export const NEW_SIZE_KEY: string = '-XX:NewSize=1536m';
export const SURVIVOR_RATIO_KEY: string = '-XX:SurvivorRatio=7';

// Constants - Others
export const CATALINA: string = 'Catalina';
export const INCLUDE_AND_OVERRIDE_PROPERTY: string = 'include-and-override';
export const BOOTSTRAP_FILE: string = 'org.apache.catalina.startup.Bootstrap';
export const INTERVAL_5000: number = 5000;
